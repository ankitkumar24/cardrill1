// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
let car = [];
function bmwAudiCar(inventory) {
    let carArr = []
    if (!Array.isArray(inventory) || inventory.length === 0) {
        return carArr
    }
    for (let index = 0; index< inventory.length; index++) {
        const name = inventory[index].car_make;
        if (name == 'BMW' || name == 'Audi') {
            car.push(inventory[index]);
        }

    }
    return car;
}

module.exports = bmwAudiCar;