let lastCar = require("../problem2.cjs");
let inventory = require("../data.cjs");

let car = lastCar(inventory);
console.log("Last car is a " + car.car_make + " " + car.car_model);