// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const years = [];
function oldCar(inventory) {
    let carArr = []
    if (!Array.isArray(inventory) || inventory.length === 0) {
        return carArr
    }
    for (let index = 0; index < inventory.length; index++) {
        const year = inventory[index].car_year;
        if (year < 2000) {
            years.push(year);
        }
    }
    return years;
}

module.exports = oldCar;